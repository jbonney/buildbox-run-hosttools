FROM registry.gitlab.com/buildgrid/buildbox/buildbox-worker:latest

COPY . /buildbox-run-hosttools

RUN cd /buildbox-run-hosttools && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make

ENV PATH "/buildbox-run-hosttools/build:$PATH"

# Build Args to set default Server and CAS Server
ARG SERVER="http://127.0.0.1:50051"
ARG CAS_SERVER=${SERVER}

# Add as ENV (to use during runtime)
ENV BUILDGRID_SERVER_URL=${SERVER}
ENV CAS_SERVER_URL=${CAS_SERVER}

# Default entry point
CMD buildbox-worker --verbose --buildbox-run=buildbox-run-hosttools --bots-remote=${BUILDGRID_SERVER_URL} --cas-remote=${CAS_SERVER_URL}
